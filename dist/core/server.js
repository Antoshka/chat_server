"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const config = require('../config');
const sign = require('../api/sign');
const checkToken = require('../middlewares/checkToken');
const app = express_1.default();
const port = 3000;
app.use(express_1.default.json());
app.use(cookie_parser_1.default());
app.use(express_1.default.static(config.staticPath));
//app.use(checkToken);
app.get('*', (request, response) => {
    response.sendFile(config.indexPath);
});
app.post('/a/sign/check', sign.checkToken);
app.post('/a/sign/up', sign.signUp);
app.post('/a/sign/in', sign.signIn);
const start = () => {
    app.listen(port, (err) => {
        if (err) {
            console.log('Server starting error', err);
        }
        else {
            console.log(`Server is listening on ${port} port.`);
        }
    });
};
module.exports = { start };
//# sourceMappingURL=server.js.map