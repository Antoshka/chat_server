"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const start = () => {
    mongoose_1.default.connect('mongodb://localhost/chat');
    mongoose_1.default.connection.once('open', function () {
        console.log('Database connection opened');
    }).on('error', function (error) {
        console.log('Database connection error:', error);
    });
};
module.exports = { start };
//# sourceMappingURL=db.js.map