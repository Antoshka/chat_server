"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config = require('../config');
module.exports = (req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
        return res.json({ status: -2, error: 'Ты не авторизован!' });
    }
    try {
        let tokenObj = jsonwebtoken_1.default.verify(token, config.secret);
    }
    catch (e) {
        return res.json({ status: -2, error: 'Ты не авторизован!' });
    }
    next();
};
//# sourceMappingURL=checkToken.js.map