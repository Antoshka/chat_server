"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config = require('../config');
const user_1 = __importDefault(require("../models/user"));
const signUp = (request, response) => {
    const { login, password } = request.body;
    if (!login || !password) {
        response.json({ status: -100, error: 'Укажи логин и пароль' });
    }
    else {
        user_1.default.findOne({ login: login }).then(function (record) {
            if (record) {
                response.json({ status: -101, error: 'Этот логин уже занят' });
            }
            else {
                const user = new user_1.default({
                    login: login,
                    password: password
                });
                user.save().then(function () {
                    response.json({
                        status: 0,
                        data: { token: jsonwebtoken_1.default.sign({ _id: user._id }, config.secret) }
                    });
                }).catch(function (e) {
                    console.log(e);
                    response.json({ status: -1, error: 'Ошибка' });
                });
            }
        }).catch(function (e) {
            console.log(e);
            response.json({ status: -1, error: 'Ошибка' });
        });
    }
};
const signIn = (request, response) => {
    const { login, password } = request.body;
    if (!login || !password) {
        response.json({ status: -100, error: 'Укажи логин и пароль' });
    }
    else {
        user_1.default.findOne({ login: login }).then(function (record) {
            if (record) {
                record.comparePasswords(password).then(res => {
                    if (res) {
                        response.json({
                            status: 0,
                            data: { token: jsonwebtoken_1.default.sign({ _id: record._id }, config.secret) }
                        });
                    }
                    else {
                        response.json({ status: -101, error: 'Логин или пароль указаны неверно' });
                    }
                }).catch(function (e) {
                    console.log(e);
                    response.json({ status: -1, error: 'Ошибка' });
                });
            }
            else {
                response.json({ status: -101, error: 'Логин или пароль указаны неверно' });
            }
        }).catch(function (e) {
            console.log(e);
            response.json({ status: -1, error: 'Ошибка' });
        });
    }
};
const checkToken = (request, response) => {
    const token = request.cookies.token;
    if (token) {
        try {
            jsonwebtoken_1.default.verify(token, config.secret);
            response.json({ status: 0 });
        }
        catch (e) {
            response.json({ status: -101, error: 'Невалидный токен' });
        }
    }
    else {
        response.json({ status: -100, error: 'Токен отсутствует' });
    }
};
module.exports = { signUp, signIn, checkToken };
//# sourceMappingURL=sign.js.map