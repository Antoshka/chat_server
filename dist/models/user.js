"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const bcrypt = require('bcrypt');
;
;
const UserSchema = new mongoose_1.Schema({
    login: { required: true, type: String, unique: true, lowercase: true },
    password: { required: true, type: String }
});
UserSchema.pre('save', function (next) {
    if (!this.isModified('password')) {
        return next();
    }
    bcrypt.hash(this.password, 10).then((hash) => {
        this.password = hash;
        next();
    });
});
UserSchema.methods.comparePasswords = function (password) {
    return bcrypt.compare(password, this.password);
};
const User = mongoose_1.model('user', UserSchema);
exports.default = User;
//# sourceMappingURL=user.js.map