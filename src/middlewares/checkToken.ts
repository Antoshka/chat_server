import { Request, Response } from 'express';
import jsonwebtoken from 'jsonwebtoken';
import config from '../config';

export default (req: Request, res: Response, next: Function) => {
	const token = req.headers['authorization'];

	if (!token) {
		return res.json({ status: -2, error: 'Ты не авторизован!' });
	}

	try {
		let tokenObj = jsonwebtoken.verify(token, config.secret);
	} catch (e) {
		return res.json({ status: -2, error: 'Ты не авторизован!' });
	}

	next();
};
