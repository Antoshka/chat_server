const path = require('path');
const clientPath = 'C:\\wamp\\www\\chat\\client';

export default {
	indexPath: path.resolve(clientPath, 'index.html'),
	staticPath: clientPath,
	secret: 'secret'
};
