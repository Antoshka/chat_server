import server from './core/server';
import db from './core/db';

server.start();
db.start();
