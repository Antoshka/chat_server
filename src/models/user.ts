import { Schema, model, Document, Model } from 'mongoose';

const bcrypt = require('bcrypt');

interface IUser {
	login: string;
	password: string;
};

interface IUserModel extends IUser, Document {
	comparePasswords: (p: string) => Promise<Boolean>
};

const UserSchema = new Schema({
	login: { required: true, type: String, unique: true, lowercase: true },
	password: { required: true, type: String }
});

UserSchema.pre<IUserModel>('save', function (next) {
	if (!this.isModified('password')) {
		return next();
	}

	bcrypt.hash(this.password, 10).then((hash: string) => {
		this.password = hash;
		next();
	});
});

UserSchema.methods.comparePasswords = function (password: string) {
	return bcrypt.compare(password, this.password);
};

const User: Model<IUserModel> = model<IUserModel>('user', UserSchema);
export default User;
