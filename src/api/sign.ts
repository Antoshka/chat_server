import { Request, Response } from 'express';
import jsonwebtoken from 'jsonwebtoken';
import config from '../config';
import User from '../models/user';

const signUp = (request: Request, response: Response) => {
	const { login, password } = request.body;

	if (!login || !password) {
		response.json({ status: -100, error: 'Укажи логин и пароль' });
	} else {
		User.findOne({ login: login }).then(function (record) {
			if (record) {
				response.json({ status: -101, error: 'Этот логин уже занят' });
			} else {
				const user = new User({
					login: login,
					password: password
				});

				user.save().then(function () {
					response.json({
						status: 0,
						data: { token: jsonwebtoken.sign({ _id: user._id }, config.secret) }
					});
				}).catch(function (e: string) {
					console.log(e);
					response.json({ status: -1, error: 'Ошибка' });
				});
			}
		}).catch(function (e: string) {
			console.log(e);
			response.json({ status: -1, error: 'Ошибка' });
		});
	}
};

const signIn = (request: Request, response: Response) => {
	const { login, password } = request.body;

	if (!login || !password) {
		response.json({ status: -100, error: 'Укажи логин и пароль' });
	} else {
		User.findOne({ login: login }).then(function (record) {
			if (record) {
				record.comparePasswords(password).then(res => {
					if (res) {
						response.json({
							status: 0,
							data: { token: jsonwebtoken.sign({ _id: record._id }, config.secret) }
						});
					} else {
						response.json({ status: -101, error: 'Логин или пароль указаны неверно' });
					}
				}).catch(function (e: string) {
					console.log(e);
					response.json({ status: -1, error: 'Ошибка' });
				});
			} else {
				response.json({ status: -101, error: 'Логин или пароль указаны неверно' });
			}
		}).catch(function (e: string) {
			console.log(e);
			response.json({ status: -1, error: 'Ошибка' });
		});
	}
};

const checkToken = (request: Request, response: Response) => {
	const token = request.cookies.token;

	if (token) {
		try {
			jsonwebtoken.verify(token, config.secret);
			response.json({ status: 0 });
		} catch (e) {
			response.json({ status: -101, error: 'Невалидный токен' });
		}
	} else {
		response.json({ status: -100, error: 'Токен отсутствует' });
	}
};

export default { signUp, signIn, checkToken };
