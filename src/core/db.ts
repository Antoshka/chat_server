import mongoose from 'mongoose';

const start = () => {
	mongoose.connect('mongodb://localhost/chat');

	mongoose.connection.once('open', function () {
		console.log('Database connection opened');
	}).on('error', function (error) {
		console.log('Database connection error:', error);
	});
};

export default { start };
