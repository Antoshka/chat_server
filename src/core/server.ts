import express from 'express';
import cookieParser from 'cookie-parser';
import config from '../config';
import sign from '../api/sign';
import checkToken from '../middlewares/checkToken';

const app = express();
const port = 3000;

app.use(express.json());
app.use(cookieParser());
app.use(express.static(config.staticPath));
//app.use(checkToken);

app.get('*', (request, response) => {
	response.sendFile(config.indexPath);
});

app.post('/a/sign/check', sign.checkToken);
app.post('/a/sign/up', sign.signUp);
app.post('/a/sign/in', sign.signIn);

const start = () => {
	app.listen(port, (err: any) => {
		if (err) {
			console.log('Server starting error', err);
		} else {
			console.log(`Server is listening on ${port} port.`);
		}
	});
};

export default { start };
